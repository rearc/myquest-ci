# Solution:
I have split my work into 2 parts, so there are 2 repositories.
One wich I call myquest-ci, and another myquest-cd.

The documentation you are looking at is the myquest-ci.
Which is mainly representative of a git repo for a Developer to change the code for the express server and go routines that are desired.

If I had more time I would develop a test bench and also add continous testing into the chain of events.

Currently this repo was just used to locally build the Docker image and test.

## Build locally

To build locally I ran the following command:

```shell
docker build . -t drgeb/myquest
```

## To run and execute locally

To execute and run this locally on my development machine which is a mac:

```shell
docker run -p3000:8080 -d drgeb/myquest
```

## To test the application

I simply created some curl scripts specfically for my linux environment to test the different
enpoints as follows:

```shell
./test/bin/test2.sh
```

```response
Congratulations! You are running in a Docker container
```

The other tests fail cause locally I am not exactly running in aws and I dont have loadbalancer.
One way to attempt to solve this is to setup an emulated environment for AWS. I just  wonder if this would work but its worth a shot!

## Push to AWS CodeCommit and build,deploy step!

Using the terraform scripts for the myquest-cd project this will setup the complete infrastructure. Once this was completed I ran the following commands:

```shell
 git remote add origin https://git-codecommit.us-east-1.amazonaws.com/v1/repos/quest-repo
 git push -u origin main -f
```

I also created a buildspec.yml file which will be used by the [AWS CodeBuild](https://docs.aws.amazon.com/codebuild/latest/userguide/build-spec-ref.html)

The buildspec will perform 2 main tasks:

1. To create the build artifacts, in this case a Docker image using docker build command.

2. To tag and push the image to the ECR docker repo.

# Notes:
## running locally, creating the Dockerfile, creating the test took me about 15min. Where I spent most of my time was setting up the AWS environment using terraform. Further details and trials of this "Quest!" documented in that repo!

# Original Problem Statement: "A quest in the clouds"

### Q. What is this quest?

It is a fun way to assess your cloud skills. It is also a good representative sample of the work we do at Rearc. Quest is a webapp made with node.js and golang.

### Q. Do I need to be an expert in node.js and golang?

No. The starting point of the quest app is `npm install && npm start`. That is all the node.js you need to know. And you wont even see golang.

### Q. So what skills should I have?

AWS. General cloud concepts, especially networking. Docker (containerization). IaC (Infrastructure as code). Linux/Unix. Git. TLS certs is a plus.

### Q. What do I have to do?

You may do all or some of the following tasks. Please read over the complete list before starting.

1. If you know how to use git, start a git repository (local-only is acceptable) and commit all of your work to it.
2. Deploy the app in AWS and navigate to the index page. Use Linux 64-bit as your OS (Amazon Linux preferred).
3. Deploy the app in a Docker container. Use `node:10` as the base image.
4. Inject an environment variable (`SECRET_WORD`) in the Docker container. The value of `SECRET_WORD` should be the secret word discovered on the index page of the application.
5. Deploy a load balancer in front of the app.
6. Use Infrastructure as Code (IaC) to "codify" your deployment. Terraform is ideal, but use whatever you know, e.g. CloudFormation, CDK, Deployment Manager, etc.
7. Add TLS (https). You may use locally-generated certs.

### Q. How do I know I have solved these stages?

Each stage can be tested as follows (where `<ip_or_host>` is the location where the app is deployed):

1. Index page (contains the secret word) - `http(s)://<ip_or_host>[:port]/`
2. Docker check - `http(s)://<ip_or_host>[:port]/docker`
3. Secret Word check - `http(s)://<ip_or_host>[:port]/secret_word`
4. Load Balancer check  - `http(s)://<ip_or_host>[:port]/loadbalanced`
5. TLS check - `http(s)://<ip_or_host>[:port]/tls`

### Q. Do I have to do all these?

You may do whichever, and however many, of the tasks above as you'd like. We suspect that once you start, you won't be able to stop. It's addictive.

### Q. What do I have to submit?

1. Your work assets, as one or both of the following:
  - A link to a hosted git repository.
  - A ZIP file containing your project directory. Include the `.git` sub-directory if you used git.
2. Proof of completion, as one or both of the following:
  - A link to a hosted AWS deployment.
  - One or more screenshots showing, at least, the index page of the final deployment.

Your work assets should include:

- IaC files, if you completed that task.
- One or more Dockerfiles, if you completed that task.
- A sensible README or other file(s) that contain instructions, notes, or other written documentation to help us review and assess your submission.

### Q. How long do I need to host my submission on AWS?

You don't have to at all if you don't want to. You can run it in AWS, grab a screenshot, then tear it all down to avoid costs.

If you _want_ to host it longer for us to view it, we recommend taking a screenshot anyway and sending that along with the link. Then you can tear down the quest whenever you want and we'll still have the screenshot. We recommend waiting no longer than one week after sending us the link before tearing it down.

### Q. What if I successfully complete all the challenges?

We have many more for you to solve as a member of the Rearc team!

### Q. What if I find a bug?

Awesome! Tell us you found a bug in your submission to us, ideally in an email, and we'll talk more!

### Q. What if I fail?

There is no fail. Complete whatever you can and then submit your work. Doing _everything_ in the quest is not a guarantee that you will "pass" the quest, just like not doing something is not a guarantee you will "fail" the quest.

### Q. Can I share this quest with others?

No.
