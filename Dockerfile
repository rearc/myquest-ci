FROM node:10

# Create app directory
WORKDIR /usr/src/app

COPY ["package.json", "package-lock.json*", "./"]

RUN npm install
RUN npm ci --only=production

COPY . .

ENV NODE_ENV=production
EXPOSE 3000
CMD [ "node", "src/000.js" ]
